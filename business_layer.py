import model_handler
from sklearn.utils.extmath import randomized_svd
import re
from os import listdir
from os.path import isfile
import pickle
import os


class Global:
    stopwords_path = './etc/StopWords.txt'
    model = None


def add_stopword(word):
    with open(Global.stopwords_path, 'a') as myfile:
        myfile.write(f'{word}\n')


def get_stopwords(pat):
    pattern = re.compile(pat)
    with open(Global.stopwords_path, 'r') as file:
        words = [word.strip('\n') for word in file if pattern.search(word)]
    return words


def remove_stopword(word):
    with open(Global.stopwords_path, 'r') as f:
        words = f.readlines()
    with open(Global.stopwords_path, 'w') as f:
        for w in words:
            if w.strip('\n') != word:
                f.write(w)


def create_model(k_val=6, num_words=10, alg=1, test_set=1000):
    if not (k_val > 0 and num_words > 0 and (alg == 0 or alg == 1)):
        raise ValueError(k_val, num_words, alg)
    if alg == 1:
        Global.model = model_handler.KMM(k_val, test_set)
    else:
        Global.model = model_handler.LDA(k_val)
    Global.model.train(db=False)
    return frequent_words(num_words)


def frequent_words(num_words=10):
    data = Global.model.encoded_data
    terms = Global.model.vectorizer.get_feature_names()

    _, _, tfidfs_2d = randomized_svd(data, n_components=Global.model.no_topics, n_iter=100, random_state=122)

    frequent_word = []

    for tfidfs in tfidfs_2d:
        terms_comp = zip(terms, tfidfs)
        sorted_terms = sorted(terms_comp, key=lambda x: x[1], reverse=True)
        frequent_word.append(sorted_terms[:num_words])

    return frequent_word


def open_model(filename, words=10):
    if os.path.exists(f'../etc/model/{filename}') is False:
        raise FileNotFoundError(filename)
    Global.model = pickle.load(open(f"./etc/model/{filename}", 'rb'))
    return frequent_words(words)


def save_model(filename):
    try:
        if os.path.exists(f"./etc/model/{filename}"):
            raise FileExistsError(filename)
        Global.model.save(filename)
    except FileExistsError as e:
        print('The file ' + str(e) + ' already exists.')


def remove_model(filename):
    if os.path.exists(f"./etc/model/{filename}"):
        os.remove(f"./etc/model/{filename}")
    else:
        return "File does not exist"


def get_all_models():
    models = [f for f in listdir("./etc/model/") if isfile(f"./etc/model/{f}")]
    return models


def predict(msg):
    return Global.model.predict(msg)


def test():
    lst = Global.model.test()
    cluster_size = 0

    for i in range(len(lst)):
        if int(lst[i][0]) > int(cluster_size):
            cluster_size = int(lst[i][0]) + 1
    return [[lst[j][1] for j in range(len(lst)) if int(lst[j][0]) == i] for i in range(cluster_size)]
