from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.decomposition import LatentDirichletAllocation
import numpy as np
import pickle
import db_connection
import pandas as pd
import preprocessing
import random


def get_data(db=False):
    df = db_connection.fetch() if db else pd.read_csv('./etc/xl.csv', header=None)
    arr = df.to_numpy()
    transposed = np.transpose(arr).tolist()

    lst = [(transposed[0][i], transposed[1][i]) for i in range(len(transposed[0]))]
    random.shuffle(lst)

    lst = [msg for msg in lst if msg[0] is not None and isinstance(msg[0], str)]
    texts = [item[0] for item in lst]
    nums = [item[1] for item in lst]

    texts = preprocessing.preprocess_messages(texts)
    return texts, nums


def train_test_split(lst, test_set):
    texts = lst[0]
    labels = lst[1]
    index = len(texts) - test_set
    return (texts[:index], labels[:index]), (texts[index:], labels[index:])


class Model:
    with open('./etc/StopWords.txt', 'r') as file:
        stopwords = file.read().splitlines()

    def train(self, db=False):
        pass

    def predict(self, text):
        pass

    def test(self):
        pass

    def save(self, filename):
        pickle.dump(self, open(f"./etc/model/{filename}.sav", 'wb'))


class KMM(Model):

    def __init__(self, k_val=6, test_set=1000):
        self.vectorizer = None
        self.no_topics = k_val
        self.training_data, self.testing_data = train_test_split(get_data(False), test_set)
        self.model = KMeans(n_clusters=k_val, init='k-means++', max_iter=100, n_init=1)

    def _vectorize(self, data):
        if self.vectorizer is None:
            self.vectorizer = TfidfVectorizer(stop_words=Model.stopwords)

        self.encoded_data = self.vectorizer.fit_transform(data)
        return self.encoded_data

    def train(self, db=False):
        self.model.fit(self._vectorize(self.training_data[0]))

    def predict(self, text):
        if self.vectorizer is None:
            raise Exception('Training the model is required before making predictions')

        y = self.vectorizer.transform([text])
        prediction = self.model.predict(y)
        return prediction[0]

    def test(self):
        texts = self.testing_data[0]
        labels = self.testing_data[1]
        predictions = [self.predict(text) for text in texts]
        return [(str(predictions[i]), str(labels[i])) for i in range(len(texts))]

    def centers(self):
        return self.model.cluster_centers_


class LDA(Model):

    def __init__(self, no_topics=6):
        self.vectorizer = None
        self.no_topics = no_topics
        self.model = LatentDirichletAllocation(learning_method='online', n_components=no_topics, random_state=0)

    def _vectorize(self, data):
        if self.vectorizer is None:
            self.vectorizer = TfidfVectorizer(stop_words=Model.stopwords)

        self.encoded_data = self.vectorizer.fit_transform(data)
        return self.encoded_data

    def train(self, db=False):
        self.model.fit(self._vectorize(get_data(db)))

    def predict(self, text):
        if self.vectorizer is None:
            raise Exception('Training the model is required before making predictions')

        y = self.vectorizer.transform([text])
        distribution = self.model.transform(y)
        (clusters, frequencies) = np.unique(distribution.argmax(axis=1), return_counts=True)
        probs = [(str(clusters[i]), str(frequencies[i] / sum(frequencies))) for i in range(len(clusters))]
        return probs
