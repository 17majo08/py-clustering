import pyodbc
from nltk.tokenize import re
import csv
import pandas as pd


def fetch():
    cnxn = pyodbc.connect("Driver={SQL Server Native Client 11.0};"
                          "Server=.;"
                          "Database=whd;"
                          "Trusted_Connection=yes;")
    cursor = cnxn.cursor()
    cursor.execute('SELECT QUESTION_TEXT, PROBLEM_TYPE_ID FROM JOB_TICKET WHERE '
                   '(PROBLEM_TYPE_ID = 3 OR PROBLEM_TYPE_ID = 10 OR PROBLEM_TYPE_ID = 11 OR PROBLEM_TYPE_ID = 12 '
                   'OR PROBLEM_TYPE_ID = 14 '
                   'OR PROBLEM_TYPE_ID = 15 OR PROBLEM_TYPE_ID = 16 OR PROBLEM_TYPE_ID = 17 OR PROBLEM_TYPE_ID = 18 '
                   'OR PROBLEM_TYPE_ID = 28 '
                   'OR PROBLEM_TYPE_ID = 29 OR PROBLEM_TYPE_ID = 30 OR PROBLEM_TYPE_ID = 48 OR PROBLEM_TYPE_ID = 50 '
                   'OR PROBLEM_TYPE_ID = 54 OR PROBLEM_TYPE_ID = 65 '
                   'OR PROBLEM_TYPE_ID = 66 OR PROBLEM_TYPE_ID = 69 OR PROBLEM_TYPE_ID = 95 '
                   'OR PROBLEM_TYPE_ID = 97 OR PROBLEM_TYPE_ID = 98 AND SEND_CLIENT_EMAIL = 0)')

    lst = [column for column in cursor]
    row_to_list = []
    for row in lst:
        row_to_list.append([str(elem) for elem in row])
    for i in row_to_list:
        with open('./etc/xl.csv', 'a', newline='') as myfile:
            writer = csv.writer(myfile)
            xx = re.sub(r'[,    \n\r]', ' ', i[0])
            writer.writerow([xx, i[1]])

    df = pd.read_csv('./etc/xl.csv')
    return df
