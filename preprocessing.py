import pickle
from nltk.stem.snowball import SnowballStemmer
from nltk.tokenize import RegexpTokenizer
from langdetect import detect

word_tokenizer = RegexpTokenizer(r'[A-Za-zÅÄÖåäöéÉ]+')
stemmer = SnowballStemmer("swedish")
stemmer_en = SnowballStemmer("english")


def __tokenize(msg):
    return word_tokenizer.tokenize(msg)


def __stem(lst):
    return [stemmer.stem(w) for w in lst]


def __stem_en(lst):
    return [stemmer_en.stem(w) for w in lst]


def __filter(lst):
    return [w for w in lst if len(w) > 1]


def _handle_en(en):
    processed_words = [__filter(__stem_en(__tokenize(msg))) for msg in en]
    en_lst = [' '.join(msg) for msg in processed_words]
    pickle.dump(en_lst, open('./etc/en.sav', 'wb'))


def _detect_lang(lst):
    en = []
    sv = []
    for m in lst:
        try:
            if detect(str(m)) == 'en':
                en.append(m)
            else:
                sv.append(m)
        except:
            print(m)
    _handle_en(en)
    return sv


def preprocess_messages(l):
    messages = [m for m in l if m is not None and isinstance(m, str)]
    processed_words = [__filter(__stem(__tokenize(msg))) for msg in messages]
    return [' '.join(msg) for msg in processed_words]
