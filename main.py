from flask import Flask
from flask import jsonify
import business_layer as bl

app = Flask(__name__)


@app.route('/openModel/<name>/<words>', methods=['GET'])
def open_model(name, words=10):
    response = jsonify(bl.open_model(name, int(words)))
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/createModel/<k>/<n>/<name>/<alg>/<test_set>', methods=['GET'])
def create_model(k, n, name, alg, test_set):
    response = jsonify(bl.create_model(k_val=int(k), num_words=int(n), alg=int(alg), test_set=int(test_set)))
    response.headers.add('Access-Control-Allow-Origin', '*')
    bl.save_model(name)
    return response


@app.route('/removeModel/<name>', methods=['GET'])
def remove_model(name):
    try:
        response = jsonify(bl.remove_model(name))
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    except FileNotFoundError as e:
        print(str(e))


@app.route('/getAllModels', methods=['GET'])
def get_all_models():
    try:
        response = jsonify(bl.get_all_models())
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    except FileNotFoundError as e:
        print(str(e))


@app.route('/predict/<text>/<title>', methods=['GET'])
def predict(text, title):
    response = jsonify(str(bl.predict(text)))
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/test', methods=['GET'])
def test():
    response = jsonify((bl.test()))
    response.headers.add('Access-Control-Allow-Origin', '*')
    print(response)
    return response


@app.route('/addStopword/<text>', methods=['GET'])
def add_stopword(text):
    bl.add_stopword(text)
    response = jsonify(f"The stopword {text} has been added to the list of stopwords")
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/removeStopword/<text>', methods=['GET'])
def remove_stopword(text):
    try:
        bl.remove_stopword(text)
        response = jsonify([f"The stopword {text} has been removed from the list of stopwords"])
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    except FileNotFoundError as e:
        print(str(e))
    except IndexError as e:
        print(str(e))


@app.route('/getStopwords/<pattern>', methods=['GET'])
def get_stopwords(pattern):
    try:
        response = jsonify(bl.get_stopwords(pattern))
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    except FileNotFoundError as e:
        print(str(e))


if __name__ == '__main__':
    app.run()
