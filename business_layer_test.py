import unittest
import business_layer as bl
from os.path import exists


class TestBusinessLayer(unittest.TestCase):
    testText = 'Hellllllllllllo!'
    nr_of_words = 10
    nr_of_clusters = 2
    nr_of_alternative_algorithm = 1
    filename = 'Jellybean'

    def test_add_stopword(self):
        bl.add_stopword(self.testText)
        getStopWord = bl.get_stopwords(self.testText)[0]
        self.assertEqual(self.testText, getStopWord)

    def test_get_stopword(self):
        list = bl.get_stopwords(self.testText)
        self.assertTrue(len(list) != 0)
        self.assertEqual(self.testText, list[0])

    def test_remove_stopword(self):
        bl.remove_stopword(self.testText)
        list = bl.get_stopwords(self.testText)
        self.assertTrue(len(list) == 0)

    def test_create_model(self):
        frequent_words = bl.create_model(k_val=nr_of_clusters, num_words=nr_of_words, alg=nr_of_alternative_algorithm)
        self.assertEquals(frequent_words, bl.frequent_words(num_words=nr_of_words))

        self.assertIsNone(bl.Global.model)
        bl.create_model(k_val=self.nr_of_clusters, num_words=self.nr_of_words,
                                              alg=self.nr_of_alternative_algorithm)
        self.assertIsNotNone(bl.Global.model)

        wrong_value = -1000
        self.assertRaises(ValueError, bl.create_model, self.nr_of_clusters, self.nr_of_words,
                          wrong_value)
        self.assertRaises(ValueError, bl.create_model, self.nr_of_clusters, wrong_value,
                          self.nr_of_alternative_algorithm)
        self.assertRaises(ValueError, bl.create_model, wrong_value, self.nr_of_words,
                          self.nr_of_alternative_algorithm)

    def test_frequent_words(self):
        frequent_words = bl.frequent_words()
        self.assertTrue(type(frequent_words) is list)
        self.assertIsNotNone(bl.frequent_words())

    def test_save_model(self):
        bl.save_model(self.filename)
        self.assertRaises(FileExistsError, bl.save_model, self.filename)

    def test_open_model(self):
        random_filename = 'Marshmello'
        previous_model = bl.Global.model
        bl.open_model(f"{self.filename}.sav", self.nr_of_words)
        current_model = bl.Global.model

        self.assertTrue(previous_model, current_model)
        self.assertRaises(FileNotFoundError, bl.open_model, f"{random_filename}.sav", self.nr_of_words)

    def test_remove_model(self):
        bl.remove_model(f"{self.filename}.sav")

        self.assertTrue(type(bl.remove_model(self.filename)) is str)
        self.assertFalse(exists(f'../../etc/model/{self.filename}.sav'))
